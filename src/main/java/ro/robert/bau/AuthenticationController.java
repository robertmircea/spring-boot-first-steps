package ro.robert.bau;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.MacSigner;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@RestController
public class AuthenticationController {
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private static final String MY_KEY = "WElcome";
    private final AuthenticationManager authenticationManager;
    private final UserDetailsService userDetailsService;

    @Autowired
    public AuthenticationController(AuthenticationManager authenticationManager, UserDetailsService userDetailsService) {
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
    }

    @RequestMapping(value = "/authenticate", method = { RequestMethod.POST })
    public UserTransfer authenticate() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Collection<? extends GrantedAuthority> grantedAuthorities = authentication.getAuthorities();
        Map<String, Boolean> roles = new HashMap<>();
        for (GrantedAuthority authority : grantedAuthorities)
            roles.put(authority.toString(), Boolean.TRUE);

        return new UserTransfer(authentication.getName(), roles, createToken(authentication));
    }

    private String createToken(Authentication authentication) {
        long issuedAt = System.currentTimeMillis() / 1000L;
        long expiresAt = issuedAt + 180L;

        String jwtClaim = String.format("{\"iss\": \"%s\", \"iat\": %d, \"exp\": %d}", authentication.getName(), issuedAt, expiresAt);
        logger.info("clear text claim: " + jwtClaim);
        Jwt encoded = JwtHelper.encode(jwtClaim, new MacSigner(MY_KEY));
        return encoded.getEncoded();
    }

    public static class UserTransfer {

        private final String name;
        private final Map<String, Boolean> roles;
        private final String token;

        public UserTransfer(String userName, Map<String, Boolean> roles, String token) {

            Map<String, Boolean> mapOfRoles = new ConcurrentHashMap<String, Boolean>();
            for (String k : roles.keySet())
                mapOfRoles.put(k, roles.get(k));

            this.roles = mapOfRoles;
            this.token = token;
            this.name = userName;
        }

        public String getName() {
            return this.name;
        }

        public Map<String, Boolean> getRoles() {
            return this.roles;
        }

        public String getToken() {
            return this.token;
        }
    }
}
