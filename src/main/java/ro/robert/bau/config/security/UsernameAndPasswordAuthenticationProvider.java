package ro.robert.bau.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

public class UsernameAndPasswordAuthenticationProvider implements AuthenticationProvider {
    @Autowired
    private UserService userService;


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String password = (String) authentication.getCredentials();
        if(password == null || password.length() == 0){
            throw new BadCredentialsException("Password was not specified or was empty");
        }

        UserDetails userDetails = userService.loadUserByUsername(authentication.getName());

        if(userDetails == null)
            throw new BadCredentialsException("Username not found");

        if(userDetails.getPassword().equals(password) ){
            return new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities());
        }

        return null;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
