package ro.robert.bau.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import ro.robert.bau.config.security.Http401UnauthorizedEntryPoint;
import ro.robert.bau.config.security.TokenAuthenticationFilter;
import ro.robert.bau.config.security.TokenAuthenticationProvider;

import javax.inject.Inject;
import java.util.Arrays;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

//    @Configuration
//    @Order(1)
//    public static class AuthenticationSecurityConfig extends  WebSecurityConfigurerAdapter{
//
//        protected AuthenticationSecurityConfig() {
//            super(true);
//        }
//
//        @Override
//        protected void configure(HttpSecurity http) throws Exception {
//            http
//                    .authenticationProvider(getUserPassAuthenticationProvider())
//                    .csrf().disable()
//                    .servletApi().and()
//                    .securityContext().and()
//                    .authorizeRequests()
//                        .antMatchers("/authenticate*")
//                        .fullyAuthenticated()
//                        .and()
//                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//                    .and()
//                    .httpBasic();
//        }
//
//        @Bean
//        public UsernameAndPasswordAuthenticationProvider getUserPassAuthenticationProvider() {
//            return new UsernameAndPasswordAuthenticationProvider();
//        }
//
//    }


    @Configuration
    public static class ApiSecurityConfig extends WebSecurityConfigurerAdapter{

        @Inject
        private Http401UnauthorizedEntryPoint alwaysUnauthorizedAuthenticationEntryPoint;


        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .authenticationProvider(getTokenAuthenticationProvider())
                    .csrf().disable()
                    .authorizeRequests()
                    .antMatchers("/authenticate").permitAll()
//                    .antMatchers("/*").authenticated()
                    .anyRequest().authenticated()
                    .and()
                    .addFilterBefore(getTokenAuthenticationFilter(), AbstractPreAuthenticatedProcessingFilter.class)
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                    .exceptionHandling().authenticationEntryPoint(alwaysUnauthorizedAuthenticationEntryPoint);
        }


        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.parentAuthenticationManager(getAuthenticationManager());
        }

        @Bean
        public TokenAuthenticationFilter getTokenAuthenticationFilter() throws Exception {
            return new TokenAuthenticationFilter(authenticationManager(), alwaysUnauthorizedAuthenticationEntryPoint);
        }

        @Bean
        public TokenAuthenticationProvider getTokenAuthenticationProvider() {
            return new TokenAuthenticationProvider();
        }

        @Bean
        public AuthenticationManager getAuthenticationManager(){
            return new ProviderManager(Arrays.<AuthenticationProvider>asList(getTokenAuthenticationProvider()));
        }

    }


//    @Configuration
//    protected static class AuthenticationConfiguration extends
//            GlobalAuthenticationConfigurerAdapter {
//
//        @Bean
//        public AuthenticationManager getAuthenticationManager(){
//            return new ProviderManager(Arrays.<AuthenticationProvider>asList(getTokenAuthenticationProvider()));
//        }
//
//        @Bean
//        public TokenAuthenticationProvider getTokenAuthenticationProvider() {
//            return new TokenAuthenticationProvider();
//        }
//        @Override
//        public void init(AuthenticationManagerBuilder auth) throws Exception {
//            auth.parentAuthenticationManager(getAuthenticationManager());
//        }
//
//    }

}