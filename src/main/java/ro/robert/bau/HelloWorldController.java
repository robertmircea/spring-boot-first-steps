package ro.robert.bau;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("/hello-world")
@Path("/welcome")
public class HelloWorldController {

    private CounterService counterService;
    private static final String template = "Hello, %s! Called by %s";
    private final AtomicLong counter = new AtomicLong();
    
    @Autowired
    public HelloWorldController(CounterService counterService){
        this.counterService = counterService;
    }

    @RequestMapping(method=RequestMethod.GET)
    @GET
    public @ResponseBody Greeting sayHello(@RequestParam(value="name", required=false, defaultValue="Stranger") @DefaultValue("Stranger") @QueryParam("name") String name) {
        counterService.increment("hello-world.invoked");

        String principal = getCurrentUser();
        return new Greeting(counter.incrementAndGet(), String.format(template, name, principal));
    }

    protected String getCurrentUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return principal instanceof UserDetails ? ((UserDetails) principal).getUsername() : principal.toString();
    }

}